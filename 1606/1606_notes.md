# Meeting 16-06-2021

## Review of *"Unconstrained Scene Generation with Locally Conditioned Radiance Fields"*
![](applepaper_pipeline.PNG)

Given a dataset of images gathered from multiple scenes, they train an ensemble of NERFs to produce a generative model of NERFs. This ensemble is encoded in a single MLP. The premise is that a latent code Z is controlling the appearence of the scene while the ray is also fed in the MLP to produce the final image in a NERF style of rendering. The latent code Z first transformed to a 2d-grid of W that encodes a latent variable for each shell of the floor. 
During training you sample a random Z and a random camera position and render an Image, this is given to the discriminator as a "fake" image, we also show the descriminator real images, and through a normal GAN training we try to match these distributions.

In principal it seems to work, this that bother me:

* There is no explicit usage of multi-view content but there is not trivial answer on how to do that.

* Style/Appearance(Z) and Camera are not explicitly disentangled.

## Single-Scene

One extension of this kind of architecture is to find a way to adjust it to perform on a single scene. This generative model would create a NERF, or an ensemble of NERFs by conditioning it to some or all of the following information:

* the camera position

* the input images projected to the novel camera

* the frustrum of the camera that can be converted to some kind of 3d-grid like the floorplan

* a visible point cloud

Then by learning to match this distribution this could probably allow the generative model to be able to synthesize radiance fields in places that has not been seen in the input views at all.

Uncertain Note: I think it is crucial for the network to be able to generalize/extrapolate to not use explicit supervision (L1/L2) losses at all even if it feels usefull since we have it.

## Multi-Scene

## Constrasting this architecture with Stavro's project
![](apple_stavros.jpg)

# Adversarial training in Point Cloud IBR

## Hypothesis

An adversarial training with patchGAN should be able to inpaint in a similar manner as patch-match by making all patches in a view match some patch from training even if it doesnt learn any real distribution but just memorize input.

## Experiments

The goal of these experiments is to get a quick glimpse on how our frame-work reacts to gradients provided by discriminators.

To avoid very hard tuning sessions I started from the pix2pixHD framework.
Changelog on pix2pixHD:

    * Exchanged their generator with ours.
    * Removed their VGG loss with an L1.
    * Generated an offline dataset of point-cloud rasterizations for each view (only color, depth-info and distortion for the average pooling layer in the middle).
    * Introduced an optional downscaling factor on our encoder architecture of a factor of 1/4 with two bilinear interpolations between the residual block layers.

I did 4 experiments:

    1. L1                   (Done)
    2. L1 + GAN             (Done)
    3. L1 + downscale       (Done)
    4. L1 + GAN + downscale (Still running)

http://nefgpu34.inria.fr:6006/
In images of the tensorboard:

* synthesized_image is views from training set.
* [TEST]synthesized_image is a test view.
* [VAL]synthesized_image_0 is view from a "hard" path.
* [VAL]synthesized_image_1 is another view from a "hard" path.

## References

### Inpainting

* EdgeConnect: Generative Image Inpainting with Adversarial Edge Learning
> One of the first deep learning methods designed for
image inpainting is context encoder [38], which uses an
encoder-decoder architecture. The encoder maps an image
with missing regions to a low-dimensional feature space,
which the decoder uses to construct the output image. However, the recovered regions of the output image often contain visual artifacts and exhibit blurriness due to the information bottleneck in the channel-wise fully connected
layer. This was addressed by Iizuka et al. [22] by reducing the number of downsampling layers, and replacing the
channel-wise fully connected layer with a series of dilated
convolution layers [51].

* Contextual-based Image Inpainting: Infer, Match, and Translate

### Conditional GANs

* High-Resolution Image Synthesis and Semantic Manipulation with Conditional GANs ( Pix2PixHD )

### To Read/Ideas

* Read Patch-Match + deep learning  + GAN
* Read Inpainting
  * ~~Image inpainting: A review: <https://arxiv.org/ftp/arxiv/papers/1909/1909.06399.pdf> (Survey)~~
  * ~~Contextual-based Image Inpainting: Infer, Match, and Translate: <https://arxiv.org/pdf/1711.08590.pdf>~~
  * ~~Coherent Semantic Attention for Image Inpainting: <https://openaccess.thecvf.com/content_ICCV_2019/papers/Liu_Coherent_Semantic_Attention_for_Image_Inpainting_ICCV_2019_paper.pdf>~~
  * ~~Texture Memory-Augmented Deep Patch-Based Image Inpainting: <https://arxiv.org/pdf/2009.13240.pdf>~~
  >The fact that our method retrieves plausible patches from a texture memory can be loosely regarded as a kind of attention. Attention mechanism [16], [17], [18] has been widely used in deep inpainting literature [4], [5], [6], [7] to extract information from valid regions
  * Zoom-to-Inpaint: Image Inpainting with High Frequency Details: <https://storage.googleapis.com/pub-tools-public-publication-data/pdf/9719628ef72743305d9d1c479634d0c7081cfe1d.pdf>
  * Super-Resolution-Based Inpainting: <https://link.springer.com/content/pdf/10.1007%252F978-3-642-33783-3_40.pdf>
  * Generative Image Inpainting with Contextual Attention: <https://openaccess.thecvf.com/content_cvpr_2018/papers/Yu_Generative_Image_Inpainting_CVPR_2018_paper.pdf>
  * Blind inpainting using the fully convolutional neural network: <https://link.springer.com/content/pdf/10.1007/s00371-015-1190-z.pdf>
  * Free-Form Image Inpainting with Gated Convolution <https://openaccess.thecvf.com/content_ICCV_2019/papers/Yu_Free-Form_Image_Inpainting_With_Gated_Convolution_ICCV_2019_paper.pdf>
  * PiiGAN (???)
  * <https://openaccess.thecvf.com/content_ICCV_2019/papers/Lee_Copy-and-Paste_Networks_for_Deep_Video_Inpainting_ICCV_2019_paper.pdf> (???)
* Read zero-shot/single/few shot learning
  * <https://openaccess.thecvf.com/content_ICCV_2019/papers/Shaham_SinGAN_Learning_a_Generative_Model_From_a_Single_Natural_Image_ICCV_2019_paper.pdf> (SinGAN)
  * <https://arxiv.org/pdf/1812.00231.pdf> (InGAN)
  * <https://openaccess.thecvf.com/content/WACV2021/papers/Hinz_Improved_Techniques_for_Training_Single-Image_GANs_WACV_2021_paper.pdf> (Improved above, 2021)
